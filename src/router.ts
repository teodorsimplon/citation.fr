import { Application } from "express";
import AuteursController from "./controllers/AuteursController";
import CitationsController from "./controllers/CitationsController";
import HomeController from "./controllers/HomeController";

export default function route(app: Application) {
    /** route to display last citation added  **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) => {
        HomeController.about(req, res);
    });

    /** route to display list of citations **/
    app.get('/citations', (req, res) => {
        CitationsController.showCitations(req, res);
    });

    /** route to display list of auteurs **/

    app.get('/auteurs', (req, res) => {
        AuteursController.showAuteurs(req, res);
    });

    // /** route to display list of citations for coresponing auteur **/
    // app.get('/auteurHasCitations', (req, res) => {
    //     CitationsController.auteurHasCitations(req, res);
    // });


    // /** route post to add citation **/
    app.get('/createCitation', (req, res) => {
        CitationsController.createCitationForm(req, res)
    });
    app.post('/createCitation', (req, res) => {
        CitationsController.createCitation(req, res)
    });


    /** route post or patch  to modify citation needs id **/

    // app.get('/citationUpdate/:id', (req, res) => {
    //     CitationsController.citationUpdateForm(req, res)
    // });

    // app.post('/citationUpdate/:id', (req, res) => {
    //     CitationsController.citationUpdate(req, res)
    // });

    // /** route delete citation needs id **/

    // app.get('/citationDelete/:id', (req, res) =>
    // {
    //     CitationsController.delete(req, res)
    // });

    // /** route post to add auteur **/

    app.get('/createAuteur', (req, res) => {
        AuteursController.createAuteurForm(req, res)
    });    
    app.post('/createAuteur', (req, res) => {
        AuteursController.createAuteur(req, res)
    });

    // /** route patch to modify auteur needs id **/

    // app.get('/auteurUpdate/:id', (req, res) =>
    // {
    //     AuteursController.auteurUpdateForm(req, res)
    // });

    app.post('/auteur-update/:id', (req, res) =>
    {
        AuteursController.updateAuteur(req, res)
    });

    // /** route delete auteur needs id **/
    app.get('/auteur-delete/:id', (req, res) => {
        AuteursController.deleteAuteur(req, res)
    });
    // app.get('/backoffice', (req, res) => {
    //     AuteursController.showBackoffice(req, res)
    // });

}
