-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Teodor
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-21 11:23
-- Created:       2022-02-21 11:18
PRAGMA foreign_keys = OFF;

-- Schema: mydb

BEGIN;
CREATE TABLE "Citations"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "auteur" VARCHAR(45) NOT NULL,
  "content" VARCHAR(45) NOT NULL
);
COMMIT;
