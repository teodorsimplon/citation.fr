import { Request, Response } from "express-serve-static-core";
import CitationsController from "./CitationsController";
import HomeController from "./HomeController";

export default class AuteursController {
  static showAuteurs(req: Request, res: Response): void {
    const db = req.app.locals.db;

    let name = req.body.name;

    let auteurs = db.prepare("SELECT * FROM Auteurs ").all();
    console.log("name exists ?: ", name);
    res.render('pages/auteurs', {
        title: 'Auteurs',
        auteurs: auteurs
    })
  }
  /** method to display list of auteurs **/

  /** method post to add auteur **/
  static createAuteurForm(req: Request, res: Response): void {
    res.render('pages/createAuteur');
  }
  static createAuteur(req: Request, res: Response): void {
      const db = req.app.locals.db;
      db.prepare('INSERT INTO Auteurs ("name") VALUES (?) ').run(req.body.name);
      CitationsController.showCitations(req, res);

  }

  /** method patch to modify auteur needs id **/

  static updateAuteur(req: Request, res: Response)
  {
      const db = req.app.locals.db;

      db.prepare('UPDATE Auteurs SET name = ?,  WHERE id = ?').run(req.body.name,  req.params.id);

      HomeController.index(req, res);
  }

  /**
   * Supprime un article
   * @param req 
   * @param res 
   */

  /** method delete auteur needs id **/
  static deleteAuteur(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM Auteurs WHERE id = ?').run(req.params.id);

        HomeController.index(req, res);
    }
}
