import { count, table } from "console";
import { Request, Response } from "express-serve-static-core";
import { FileWatcherEventKind } from "typescript";

export default class CitationsController {

    /** method to display list of citations **/
    static showCitations(req: Request, res: Response) {
        const db = req.app.locals.db;

        const citations = db.prepare('SELECT * FROM Citations').all();

        res.render('pages/citations', {
            title: 'Citations',
            Citations: citations
        });
    }

    /**
     * @param req 
     * @param res 
     */


    /** method to display list of citations for coresponing auteur **/


    /** method post to add citation **/
    static createCitation(req: Request, res: Response): void {
      
        const db = req.app.locals.db;
        db.prepare('INSERT INTO Citations ("content", "Auteurs_id") VALUES (?,?)').run(req.body.content, req.body.citationsAuteur ); 

       }

 
        
    

    /**
     * Recupere le formulaire et insere l'article en db
     * @param req 
     * @param res 
     */

    static createCitationForm(req: Request, res: Response): void {

        const db = req.app.locals.db;
       const citationAuteur = db.prepare("SELECT * FROM Auteurs").all()
        res.render('pages/createCitation', {
            auteurs: citationAuteur
        });


    }
    // show backoffice 
    static showBackoffice(req: Request, res: Response): void {

        // const db = req.app.locals.db;
        // const auteurs = db.prepare("SELECT * FROM Auteurs")
        res.render('pages/backoffice')


    }
    /** method post or patch  to modify citation needs id **/

    static citationUpdate(req: Request, res: Response): void {

    }

    /** method delete citation needs id **/


}
