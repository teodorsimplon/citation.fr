import { Request, Response } from "express-serve-static-core";

export default class HomeController
{
    /** method get last posted citation **/
    static index(req: Request, res: Response): void
    {
        res.render('pages/index', {
            title: 'Welcome',
        });
    }

    static about(req: Request, res: Response): void
    {
        res.render('pages/about', {
            title: 'About',
        });
    }
}